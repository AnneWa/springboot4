package co.cda_simplon.springBoot4.Service;

import co.cda_simplon.springBoot4.Model.Book;
import co.cda_simplon.springBoot4.Repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public Optional<Book> getBookById(final Long id) {
        return bookRepository.findById(id);
    }

    public Iterable<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public void deleteBookById(final Long id) {
        bookRepository.deleteById(id);
    }

    public Book saveOrUpdateBook(Book book) {
        return bookRepository.save(book);
    }

    public Iterable<Book> getBookByTitle(String title){
        return bookRepository.findByTitle(title);
    }
}
