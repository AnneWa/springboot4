package co.cda_simplon.springBoot4.Service;

import co.cda_simplon.springBoot4.Model.Author;
import co.cda_simplon.springBoot4.Repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthorService {
    @Autowired
    private AuthorRepository authorRepository;

    public Iterable<Author> getAuthors() {
        return authorRepository.findAll();
    }

    public Optional<Author> getAuthorById(final Long id) {
        return authorRepository.findById(id);
    }

    public Author saveOrUpdateAuthor(Author author) {
        return authorRepository.save(author);
    }

    public void deleteById(final Long id) {
        authorRepository.deleteById(id);
    }

    public Iterable<Author> getBooksByAuthorId(Integer authorId){
        return authorRepository.findBooksByAuthor(authorId);
    }
}
