package co.cda_simplon.springBoot4.Service;

import co.cda_simplon.springBoot4.Model.Category;
import co.cda_simplon.springBoot4.Repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public Optional<Category> getCategoryById(final Long id) {
        return categoryRepository.findById(id);
    }

    public Iterable<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    public void deleteCategoryById(final Long id) {
        categoryRepository.deleteById(id);
    }

    public Category saveOrUpdateCategory(Category category) {
        return categoryRepository.save(category);
    }
}
