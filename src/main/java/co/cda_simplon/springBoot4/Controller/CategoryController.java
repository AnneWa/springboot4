package co.cda_simplon.springBoot4.Controller;

import co.cda_simplon.springBoot4.Model.Category;
import co.cda_simplon.springBoot4.Service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/categories")
    public Iterable<Category> getAllCategories() {
        return categoryService.getAllCategories();
    }

    @GetMapping("/categories/{id}")
    public Category getCategoryById(@PathVariable("id") final Long id) {
        Optional<Category> c = categoryService.getCategoryById(id);
        if (c.isPresent()) {
            return c.get();
        } else {
            return null;
        }
    }

    @DeleteMapping("/categories/{id}")
    public String deleteCategoryById(@PathVariable("id") final Long id) {
        categoryService.deleteCategoryById(id);
        return "category: " + getCategoryById(id).getName() + "deleted.";
    }

    @PostMapping("/categories")
    @ResponseBody
    public Category createCategory(@RequestBody Category category) {
        return categoryService.saveOrUpdateCategory(category);
    }

    @PutMapping("/categories/{id}")
    @ResponseBody
    public Category updateCategory(@PathVariable("id") final Long id) {
        Optional<Category> c = categoryService.getCategoryById(id);
        if (c.isPresent()) {
            Category currentCategory = c.get();

            String name = currentCategory.getName();
            if (name != null) {
                currentCategory.setName(name);
            }
            categoryService.saveOrUpdateCategory(currentCategory);
            return currentCategory;
        } else {
            return null;
        }
    }
}

