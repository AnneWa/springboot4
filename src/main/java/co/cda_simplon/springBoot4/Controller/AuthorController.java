package co.cda_simplon.springBoot4.Controller;

import co.cda_simplon.springBoot4.Model.Author;
import co.cda_simplon.springBoot4.Model.Book;
import co.cda_simplon.springBoot4.Service.AuthorService;
import co.cda_simplon.springBoot4.Service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class AuthorController {
    @Autowired
    private AuthorService authorService;

    @Autowired
    private BookService bookService;

    @GetMapping("authors")
    @ResponseBody
    public Iterable<Author> getAllAuthors(){
        return authorService.getAuthors();
    }

    @GetMapping("/authors/{authorId}")
    public Optional<Author> getAuthorById(@PathVariable Long authorId) {
        Optional<Author> optionalAuthor = authorService.getAuthorById(authorId);
        if (optionalAuthor.isPresent()) {
            return optionalAuthor;
        } else {
            return null;
        }
    }

    @PostMapping("/authors")
    public Author createAuthor(@RequestBody Author author){
        return authorService.saveOrUpdateAuthor(author);
    }


    @GetMapping("/authors/{authorId}/books")
    public Iterable<Book> getBooksByAuthorId(@PathVariable Long authorId){
        Optional<Author> optionalAuthor = authorService.getAuthorById(authorId);
        //Iterable<Book> books = bookService.getAllBooks();
        if (optionalAuthor.isPresent()) {
            Iterable<Book> authorsBooks = optionalAuthor.get().getBooks();
            return authorsBooks;
        }else {
            return null;
        }
    }





}
