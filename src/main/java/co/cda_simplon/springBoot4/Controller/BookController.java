package co.cda_simplon.springBoot4.Controller;

import co.cda_simplon.springBoot4.Model.Book;
import co.cda_simplon.springBoot4.Model.Category;
import co.cda_simplon.springBoot4.Service.BookService;
import co.cda_simplon.springBoot4.Service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/books/title/{title}")
    public Iterable<Book> getBookByTitle(@PathVariable String title){
        return bookService.getBookByTitle(title);
    }

    @GetMapping("/books/{idBook}/category/{idCategory}")
    @ResponseBody
    public Book bookCategory(@PathVariable Long idBook,
                             @PathVariable Long idCategory) {
        Optional<Book> optionalBook = bookService.getBookById(idBook);
        Optional<Category> optionalCategory = categoryService.getCategoryById(idCategory);

        if (optionalBook.isPresent() && optionalCategory.isPresent()){
            Book book = optionalBook.get();
            Category category = optionalCategory.get();

            book.setCategory(category);
            return bookService.saveOrUpdateBook(book);
        }
        return null;
    }

    @GetMapping("/books/{id}")
    public Book getBookById(@PathVariable("id") final Long id) {
        Optional<Book> b = bookService.getBookById(id);
        if (b.isPresent()) {
            return b.get();
        } else {
            return null;
        }
    }

    @GetMapping("books")
    public Iterable<Book> getAllBooks() {
        return bookService.getAllBooks();
    }

    @DeleteMapping("books/{id}")
    public String deleteBookById(@PathVariable("id") final Long id) {
        bookService.deleteBookById(id);
        return "The book has been deleted.";
    }

    @PostMapping("/books")
    @ResponseBody
    public Book createBook(@RequestBody Book book) {
        return bookService.saveOrUpdateBook(book);
    }

    @PutMapping("books/{id}")
    @ResponseBody
    public Book updateBook(@PathVariable("id") final Long id, @RequestBody Book book) {
        Optional<Book> b = bookService.getBookById(id);
        if (b.isPresent()) {
            Book currentBook = b.get();

            String title = book.getTitle();
            if (title != null) {
                currentBook.setTitle(title);
            }
            String description = book.getDescription();
            if (description != null) {
                currentBook.setDescription(description);
            }
            boolean available = book.isAvailable();
            if (!available) {
                currentBook.setAvailable(false);
            }
            bookService.saveOrUpdateBook(currentBook);
            return currentBook;
        } else {
            return null;
        }
    }


}
