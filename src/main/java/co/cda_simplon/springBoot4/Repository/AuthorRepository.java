package co.cda_simplon.springBoot4.Repository;

import co.cda_simplon.springBoot4.Model.Author;
import co.cda_simplon.springBoot4.Model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

    public Iterable<Author> findBooksByAuthor(Integer authorId);
}
