package co.cda_simplon.springBoot4.Repository;

import co.cda_simplon.springBoot4.Model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
